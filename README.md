# README #
Testbooking client. A simple site which renders flight destinations from a given city, renders promoboxes and other stuff

### How do I get set up? ###

npm install

To run a local instance on port 8080, type
npm run dev

### What is here ###
So currently is working:
1. Navigation. It uses react routes to change pages. All pages are separate components, but contains only placeholders (except the main page)
2. Promoboxes. Data is taken from the server, so works fully
3. Search with autocompletion. Data is taken from the server (airpots data). Works fully
4. TripFounder. Once search's autocompletion item is obtained, starts to interact with server to get more information about the trip. MVP: interaction works OK, but due to lack of time, the most of functionality was not implemented

### How to use this site ###
Simply start typing any city name in the search, get results and see airports and schedule in flights section. Cities are taken from a raw json, so not sorted at all (typing 'Lon' will show some small cities first). This should be made on backend, so I didn't implement that
