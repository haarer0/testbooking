import React from "react";

import TripFounder, { TRIPFOUNDER_CATEGORIES } from '../components/TripFounder/TripFounder'
import TripFounderCategoryContainer from '../components/TripFounder/TripFounderCategoryContainer'
import PromoboxesSection from '../components/Promoboxes/PromoboxesSection'
import SearchWithAutocompletion from '../components/Search/SearchWithAutocompletion'

//main page component with Autocompletion Search, TripFounder and promoboxesSection components on it
export default class Main extends React.Component {
  render() {

    return (
      <div>
        <div class="row">
          <div class="col-lg-12">
            <div class="row">
              <SearchWithAutocompletion
                componentId="search-whereToGo"
                title="Where you want to go?"
                placeholder="Miami"
                minInputCharactersToAutocomplete={3} //how many characters are required to begin auto completion, default is 3
                maxAutocompletionResults={5}  //how many autocompletion results should be received from server, default is 5
                searchType="cities" //search through cities
                wrapperStyle={{position: 'relative'}}
                formatInputText={(title, subtitle) => title + (subtitle ? ', ' + subtitle : '')}
              />
            </div>
            <div class="row">
              <TripFounder
                citiesSearchComponentId="search-whereToGo"
              >
                <TripFounderCategoryContainer title="Flights" dataPropertyName={ TRIPFOUNDER_CATEGORIES.FLIGHTS }> { Math.random() } </TripFounderCategoryContainer>
                <TripFounderCategoryContainer title="Hotels" dataPropertyName={ TRIPFOUNDER_CATEGORIES.HOTELS }> { Math.random() } </TripFounderCategoryContainer>
                <TripFounderCategoryContainer title="Cars" dataPropertyName={ TRIPFOUNDER_CATEGORIES.CARS }> { Math.random() } </TripFounderCategoryContainer>
                <TripFounderCategoryContainer title="Offers" dataPropertyName={ TRIPFOUNDER_CATEGORIES.OFFERS }> { Math.random() } </TripFounderCategoryContainer>
              </TripFounder>
            </div>
          </div>
        </div>
        <PromoboxesSection
          componentId="promoboxes-main"
          cellClass="col-lg-4" //each promobox' container will use this CSS classname
          amount={3} //how many promoboxes to render, default is 3
          autoUpdateTimeSeconds={-1} //put positive value (in seconds) to start auto update, default is -1 (disabled)
          isRandom={true} //ask server to return random promobox on each request, default is true
        />
      </div>
    );
  }
}

/*
  <Tabs tabsId="whereToGoSearchResults">
    <div title="Flights" >Find a flight</div>
    <div title="Hotels" >Hotels content</div>
    <div title="Cars" >Cars content</div>
    <div title="Offers" >Offers content</div>
  </Tabs>
*/
