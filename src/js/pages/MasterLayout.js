import React from "react";

import Navigation from '../components/Navigation/Navigation'
import Footer from '../components/Footer/Footer'

//master layout is used to render all pages (except 404)
export default class MasterLayout extends React.Component {
  render() {
    return (
      <div>
        <Navigation location={this.props.location} />

        <div class="container" style={{margin: '80px auto'}}>
          {this.props.children}
        </div>

        <Footer/>
      </div>
    );
  }
}
