import React from "react";

import Navigation from '../components/Navigation/Navigation'
import Footer from '../components/Footer/Footer'

export default class Page404 extends React.Component {
  render() {
    const { location } = this.props;
    return (
      <div>
        <Navigation location={location} />

        <div class="container" style={{marginTop: '60px'}}>
          <div class="row">
            <div class="col-lg-12">
              <h3>No match for <code>{location.pathname}</code></h3>
            </div>
          </div>
          <Footer/>
        </div>
      </div>
    );
  }
}
