
import axios from "axios";

export default function reducer(state={
    //this prop is used to save states per component, using their componentId as a key
    components: {}
  }, action) {

    switch (action.type) {
      case "PROMOBOXES_FETCH_DATA_PENDING": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              isFetching: true,
              err: null
            }
          }
        }
      }
      case "PROMOBOXES_FETCH_DATA_REJECTED": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              isFetching: false,
              isFetched: false,
              err: action.payload.error
            }
          }
        }
      }
      case "PROMOBOXES_FETCH_DATA_FULFILLED": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              isFetching: false,
              isFetched: true,
              promoboxes: action.payload.data,
              err: null
            }
          }
        }
      }
    }

    return state
}
