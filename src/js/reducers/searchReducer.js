import axios from "axios";

const initialState = {
}

export default function reducer(state={
    //this prop is used to save states per component, using their componentId as a key
    components: {}
  }, action) {

    switch (action.type) {
      case "SEARCH_FETCH_AUTOCOMPLETION_PENDING": {
        return {
          ...state,
          components: {
            ...state.components,
            //storing received data into {components: {COMPONENTID: {...}}} object, so multiple instances can get only their data sub-object
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              autoCompletion: {
                isFetching: true,
                isFetched: false,
              },
              searchResults: {
                isFetched: false,
                data: []
              }
            }
          }
        }
      }
      case "SEARCH_FETCH_AUTOCOMPLETION_FULFILLED": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              autoCompletion: {
                isFetching: false,
                isFetched: true,
                data: action.payload.data.results,
                error: null
              }
            }
          }
        }
      }
      case "SEARCH_FETCH_AUTOCOMPLETION_REJECTED": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              autoCompletion: {
                isFetching: false,
                isFetched: false,
                data: [],
                error: action.payload.error
              }
            }
          }
        }
      }
      case "SEARCH_CLEAR_AUTOCOMPLETION": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              autoCompletion: {
                isFetching: false,
                isFetched: false,
                data: [],
                error: null
              }
            }
          }
        }
      }

      case "SEARCH_ITEM_SELECTED": {
        return {
          ...state,
          components: {
            ...state.components,
            [action.payload.componentId]: {
              ...state.components[action.payload.componentId],
              searchResults: {
                isFetching: false,
                isFetched: true,
                data:  action.payload.foundData,
                error: null
              }
            }
          }
        }
      }
    }

    return state
}
