import axios from "axios";

import { fetchFoundTripData } from '../actions/tripFounderActions'

export default function reducer(state={
    //reducer doesn't have components object, assuming we may not have more than one tripFounder component on a page
    searchData: {},
    tripData: {}
  }, action) {

  switch (action.type) {
    case "SEARCH_ITEM_SELECTED": {
      return {
        ...state,
        searchData: {
          [action.payload.componentId]: {
            isCitySelected: true,
            cityId: action.payload.foundData.id
          }
        }
      }
    }

    case "TRIPFOUNDER_FETCH_CITYDATA_PENDING": {
      return {
        ...state,
        //setting searchData to null, tellingthe components that we are no longer user searchresults, but tripData
        searchData: null,
        tripData: {
          isFetching: true,
          isFetched: false
        },
      }
    }

    case "TRIPFOUNDER_FETCH_CITYDATA_FULFILLED": {
      return {
        ...state,
        searchData: null,
        tripData: {
          isFetching: false,
          isFetched: true,
          data: action.payload
        },
      }
    }

    case "TRIPFOUNDER_FETCH_CITYDATA_REJECTED": {
      return {
        ...state,
        searchData: null,
        tripData: {
          isFetching: false,
          isFetched: false,
          error: action.payload
        },
      }
    }
  }

  return state
}
