import { combineReducers } from "redux"

import promoboxesReducer from "./promoboxesReducer"
import searchReducer from "./searchReducer"
import tripFounderReducer from "./tripFounderReducer"

export default combineReducers({
  promoboxesReducer,
  searchReducer,
  tripFounderReducer
})
