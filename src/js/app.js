import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import {IntlProvider} from 'react-intl';

import { Provider } from "react-redux"

import MasterLayout from './pages/MasterLayout.js'
import Main from './pages/Main.js'
import Flights from './pages/Flights.js'
import Hotels from './pages/Hotels.js'
import Cars from './pages/Cars.js'
import Discover from './pages/Discover.js'
import Offers from './pages/Offers.js'

import Page404 from './pages/Page404.js'

import store from "./store"

const app = document.getElementById('app');

//https://www.expedia.com

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale="en">
      <Router history={hashHistory}>
          <Route path="/" component={MasterLayout}>
            <IndexRoute component={Main}></IndexRoute>
            <Route path="/flights" component={Flights}></Route>
            <Route path="/hotels" component={Hotels}></Route>
            <Route path="/cars" component={Cars}></Route>
            <Route path="/discover" component={Discover}></Route>
            <Route path="/offers" component={Offers}></Route>
          </Route>
      </Router>
    </IntlProvider>
  </Provider>,
app);
