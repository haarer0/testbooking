export const SETTINGS_API_VERSION = '' //we can easily switch between endpoint api versions by providing extra endpoint subroute here, eg '/v2'
export const SETTINGS_API_ENDPOINT = 'http://localhost:3000'
export const SETTINGS_API_LAG_SIMULATOR_ENABLED = true //options that emulates network lags
export const SETTINGS_API_LAG_SIMULATOR_MIN = 200 //min network lag, in ms. doesn't work unless SETTINGS_API_LAG_SIMULATOR_ENABLED is set to true
export const SETTINGS_API_LAG_SIMULATOR_MAX = 500 //max network lag, in ms. doesn't work unless SETTINGS_API_LAG_SIMULATOR_ENABLED is set to true

export const SETTINGS_API_PROMOBOX_ENDPOINT = '/promoboxes'
export const SETTINGS_API_SEARCH_ENDPOINT = '/search'
export const SETTINGS_API_TRIPFOUNDER_ENDPOINT = '/trip'


//helper function that resolves endpoints and applies extra query parameters, e.g. lag simulator
export function generateAPIEndpoint(apiCategory, query = null) {
  let q = '?';
  if (query)
    q += Object.keys(query).reduce((a, k) => { a.push(k + '=' + encodeURIComponent(query[k])); return a }, []).join('&') + '&';
    /*
    this converts query object { param1: 'val1', param2: 'val2' } into param1=val1&param2=val2
    */


  return SETTINGS_API_ENDPOINT + SETTINGS_API_VERSION + apiCategory +
    (SETTINGS_API_LAG_SIMULATOR_ENABLED ?
      `${q}lagRandomMin=${SETTINGS_API_LAG_SIMULATOR_MIN}&lagRandomMax=${SETTINGS_API_LAG_SIMULATOR_MAX}` : '')
}
