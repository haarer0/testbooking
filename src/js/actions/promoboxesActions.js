import axios from "axios";

import { generateAPIEndpoint, SETTINGS_API_PROMOBOX_ENDPOINT} from "../settings/api";

export function fetchPromoboxes(componentId, amount = 3, isRandom = true) {
  return function(dispatch) {
    dispatch({type: "PROMOBOXES_FETCH_DATA_PENDING", payload: {componentId}})

    let query = {
      amount,
      ...(isRandom ? {random: '1'} : {}) //set random:'1' property if isRandom == true
    }

    axios.get(generateAPIEndpoint(SETTINGS_API_PROMOBOX_ENDPOINT, query))
      .then((response) => {
        dispatch({type: "PROMOBOXES_FETCH_DATA_FULFILLED", payload: {componentId, data: response.data}})
      })
      .catch((err) => {
        dispatch({type: "PROMOBOXES_FETCH_DATA_REJECTED", payload: {componentId, data: [], error: err}})
      })
  }
}
