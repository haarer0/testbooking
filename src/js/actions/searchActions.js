import axios from "axios";

import { generateAPIEndpoint, SETTINGS_API_SEARCH_ENDPOINT } from "../settings/api";

export function fetchAutoCompletion(componentId, searchType, searchTerm, maxResults) {
  return function(dispatch) {
    dispatch({type: "SEARCH_FETCH_AUTOCOMPLETION_PENDING", payload: {componentId, searchTerm}});

    const query = {searchTerm, maxResults, isAutoCompletion: 1}
    //different search components may have point to specific searchType subpath
    const searchEndpoint = SETTINGS_API_SEARCH_ENDPOINT + (searchType.startsWith('/') ? '' : '/') + searchType

    axios.get(generateAPIEndpoint(searchEndpoint, query))
      .then((response) => {
        dispatch({type: "SEARCH_FETCH_AUTOCOMPLETION_FULFILLED", payload: {componentId, data: response.data}})
      })
      .catch((err) => {
        dispatch({type: "SEARCH_FETCH_AUTOCOMPLETION_REJECTED", payload: {componentId, data: [], error: err}})
      })
  }
}

export function clearAutoCompletion(componentId) {
  return function(dispatch) {
    dispatch({type: "SEARCH_CLEAR_AUTOCOMPLETION", payload: {componentId}});
  }
}

//when user selects an item from autocompletion search, meaning they have "found" an item
export function onSearchItemSelected(componentId, searchType, foundData) {
  return function(dispatch) {
    dispatch({type: "SEARCH_ITEM_SELECTED", payload: {componentId, searchType, foundData}});
  }
}

/*
export function searchById(componentId, searchType, searchItemId) {
  return function(dispatch) {
    dispatch({type: "SEARCH_FETCH_SEARCHBYID_PENDING", payload: {componentId}});

    const query = {searchItemId}
    const searchEndpoint = SETTINGS_API_SEARCH_ENDPOINT + (searchType.startsWith('/') ? '' : '/') + searchType

    axios.get(generateAPIEndpoint(searchEndpoint, query))
      .then((response) => {
        dispatch({type: "SEARCH_FETCH_SEARCHBYID_FULFILLED", payload: {componentId, data: response.data}})
      })
      .catch((err) => {
        dispatch({type: "SEARCH_FETCH_SEARCHBYID_REJECTED", payload: {componentId, data: [], error: err}})
      })
  }
}
*/
