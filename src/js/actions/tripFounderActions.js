import axios from "axios"

import { generateAPIEndpoint, SETTINGS_API_TRIPFOUNDER_ENDPOINT } from "../settings/api";

//getting trip data from the server to the selected destination city
export function fetchFoundTripData(cityId) {
  return function(dispatch) {
    dispatch({type: "TRIPFOUNDER_FETCH_CITYDATA_PENDING", payload: {cityId}});

    axios.get(generateAPIEndpoint(SETTINGS_API_TRIPFOUNDER_ENDPOINT, {cityId}))
      .then((response) => {
        dispatch({type: "TRIPFOUNDER_FETCH_CITYDATA_FULFILLED", payload: response.data})
      })
      .catch((err) => {
        dispatch({type: "TRIPFOUNDER_FETCH_CITYDATA_REJECTED", payload:  {error: err}})
      })
  }
}
