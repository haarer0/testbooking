import React from "react";
import {FormattedRelative} from 'react-intl';
import { Link } from "react-router";

//single promobox. it can't get its own data from server, but controlled from promoboxes section component
export default class Promobox extends React.Component {
  renderImage(image) {
    return image ? (<img style={{ height: '200px', width: '100%', display: 'block' }} src={image.src} alt={image.alt} />) : null;
  }

  renderList(list) {
    return list ? (
      <ul class="list-group list-group-flush">
        {list.map((el, index) => (
          <li class="list-group-item" key={index}>
            <Link class="card-link" to={el.link} key={index}>{el.title}</Link>
          </li>
        ))}
      </ul>
    ) : null;
  }

  renderLinks(links) {
    return links ? (
      <div class="card-body">
        { links.map((el, index) => <Link class="card-link" to={el.link} key={index}>{el.title}</Link>)}
      </div>
    ) : null;
  }

  renderDate(date) {
    //using React.Intl's FormattedRelative to render date as string like '5 days ago'
    return date ? (
      <div class="card-footer text-muted">
        <FormattedRelative value={date} />
      </div>
    ) : null;
  }

  render() {
    const {data} = this.props;

    return (
      <div class="card mb-3">
        { this.renderImage(data.image) }

        <div class="card-body">
          <p class="card-text">{data.content}</p>
        </div>

        { this.renderList(data.list) }
        { this.renderLinks(data.links) }
        { this.renderDate(data.date) }
      </div>
    );
  }
}
