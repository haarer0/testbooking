import React from "react";
import { connect } from "react-redux"

import { fetchPromoboxes } from '../../actions/promoboxesActions'

import Promobox from './Promobox'

@connect((store, ownProps) => {
  if (!store.promoboxesReducer.components[ownProps.componentId])
    store.promoboxesReducer.components[ownProps.componentId] = {}

  return {
    data: store.promoboxesReducer.components[ownProps.componentId]
  }
})
//this component renders promoboxes items, controls their amount and autoUpdating over time
export default class PromoboxesSection extends React.Component {
  //keeping the timer handler so we can clean up once component will unmount
  updateIntervalHandler = null

  componentWillMount() {
    if (!this.props.data.isFetched) {
      this.props.dispatch(fetchPromoboxes(this.props.componentId, this.props.amount, this.props.isRandom));
    }

    if (!this.updateIntervalHandler && (this.props.autoUpdateTimeSeconds > 0)) {
      this.updateIntervalHandler = setInterval(() => {
        this.props.dispatch(fetchPromoboxes(this.props.componentId, this.props.amount, this.props.isRandom));
      }, this.props.autoUpdateTimeSeconds * 1000)
    }
  }

  componentWillUnmount() {
    clearInterval(this.updateIntervalHandler);
  }

  render() {
    if ( this.props.data.isFetched) {
      //once the data is fetched, we are free to render this section's promoboxes
      return (
        <div class="row">
          {this.props.data.promoboxes.map((el, index) =>
            <div class={this.props.cellClass} key={index}>
              <Promobox data={el} key={index} />
            </div>
          )}
        </div>
      )
    } else if ( this.props.data.isFetching){
      //promoboxes are loading. it is a place where we can put some fancy loading icon
      return (
        <div class="is-loading">Loading promoboxes</div>
      )
    } else {
      return null
    }
  }
}


PromoboxesSection.propTypes = {
  componentId:  React.PropTypes.string.isRequired, //each component, which we expect may be reused on page should have a unique id, which binds to its data in store
  amount: React.PropTypes.number,
  isRandom: React.PropTypes.bool,
  autoUpdateTimeSeconds:  React.PropTypes.number
}

PromoboxesSection.defaultProps = {
  amount: 3, //how many promoboxes we need to get from server and render
  isRandom: true, //should get random promoboxes from the server ?
  autoUpdateTimeSeconds: -1 //positive values to enable autoUpdating, in seconds
}
