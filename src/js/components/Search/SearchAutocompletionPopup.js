import React from "react";
import { connect } from "react-redux"

@connect((store, ownProps) => {

  //setting up default data if we didn't receive any search information yet
  if (!store.searchReducer.components[ownProps.searchComponentId])
    store.searchReducer.components[ownProps.searchComponentId] = {autoCompletion: {isFetching: false, isFetched: false, data: []}}
  return {
    autocompletionState: store.searchReducer.components[ownProps.searchComponentId].autoCompletion
  }
})
//this is a list of popups, with handlers when one of them is clicked
export default class SearchAutocompletionPopup extends React.Component {
  isClicked = false //both onMouseDown and OnTouchStart calls handleClick on mobile responsive view with mouse cursor enabled. this will filter to make only one click

  handleClick(e) {
    if (this.isClicked)
      return

    this.isClicked = true; //later onBlur will be triggered in parent component so this list will be hidden and the flag will be reset
    this.props.onItemSelected(e.target.dataset.resultid) //calling th parent event handler
  }


  renderSubTitle(subtitle, cssClass='text-muted') {
    if (subtitle)
      return (
        <small style={{marginLeft:'5px'}} class={cssClass}>{subtitle}</small>
      )

    return null
  }

  renderList(results) {
    this.isClicked = false //resetting clicked flag every time we re-render this list.
    if (!results.length)
      return (
        <p class="list-group-item list-group-item-action" key={0} data-resultid={-1}>
          <span>Nothing found</span>
        </p>
      )


    /*
    we can't use onClick as it is not called due to onBlur higher priority call
    https://stackoverflow.com/a/28963938 according to this suggestion, onMouseDown has higher priority
    Although we need to add ontouchstart event for sake of consistency on mobiles/tablets
    */
    return results.map((el, index) => {
      return (
        <p class="list-group-item list-group-item-action" key={index} data-resultid={el.id} onMouseDown={this.handleClick.bind(this)} onTouchStart={this.handleClick.bind(this)}>
          <span>{el.title}</span>
          { this.renderSubTitle(el.subtitle) }
        </p>
      )
    })
  }

  render() {
    const autocompletionState = this.props.autocompletionState || {} //the fastest way according to https://stackoverflow.com/a/41532415
    
    if (this.props.isFocused && autocompletionState.isFetched)
      return (
        <div class="list-group" style={{position: 'absolute', zIndex:'1000', textAlign: 'left', top:'98px', marginTop:'-1px',width: '100%', maxWidth: '400px', cursor: 'pointer'}}>
          { this.renderList(autocompletionState.data || []) }
        </div>
      )

    return null
  }
}

SearchAutocompletionPopup.propTypes = {
  searchComponentId:  React.PropTypes.string.isRequired, //parent search componentId, is required to bind this data
  onItemSelected:  React.PropTypes.func.isRequired, //a callback to click handler
  isFocused: React.PropTypes.bool
};
SearchAutocompletionPopup.defaultProps = {
  isFocused: false,
}
