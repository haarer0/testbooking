import React from "react";
import { connect } from "react-redux"

import SearchAutocompletionPopup from './SearchAutocompletionPopup'

import { fetchAutoCompletion, onSearchItemSelected, clearAutoCompletion } from '../../actions/searchActions'

@connect((store, ownProps) => {
  if (!store.searchReducer.components[ownProps.componentId])
    store.searchReducer.components[ownProps.componentId] = {}
  return {
    data: store.searchReducer.components[ownProps.componentId]
  }
})
export default class SearchWithAutocompletion extends React.Component {
  autocompletionTimerHandler = null
  autocompletionPopupComponent = null

  constructor(props, context) {
    super(props, context)

    //storing the input value to state, so we can modify it programmatically once user selects an item
    this.state = { searchInputValue: '' }
  }

  componentWillMount() {
    const searchResults = this.props.data.searchResults
    if (searchResults && searchResults.isFetched)
      this.changeInput(searchResults.data.title, searchResults.data.subtitle) //use stored value (e.g. if we return from another page, and we have already selected an item from list)
  }

  componentWillUnmount() {
    this.stopAutocompletion()
  }

  startAutocompletion(searchTerm) {
    this.stopAutocompletion()

    //making a delay before start looking for suggested autocompletion results, this may reduce amount of calls to the server if users prints characters quite fast
    this.autocompletionTimerHandler = setTimeout(() => {
      //make an autocompletion search when debounced
      this.props.dispatch(fetchAutoCompletion(this.props.componentId, this.props.searchType, this.prepareSearchTerm(searchTerm), this.props.maxAutocompletionResults))
      //as fired timer handlers are just INT types, need to clean up the value manually
      this.stopAutocompletion()
    }, this.props.searchAutocompletionDelay)
  }

  stopAutocompletion() {
    clearTimeout(this.autocompletionTimerHandler)
  }

  //this method converts the input to server search engine-friendly type, e.g. London, GB => London. server will convert terms to lowercase itself
  prepareSearchTerm(input) {
    return input ? input.split(',')[0] : ''
  }

  //convert search input value to user-friendly string from a callback function. We use title and subtitle values, as autocompletion may suggest country name next to the selected city
  changeInput(title, subtitle = null) { //easy way to format and update search input
    this.setState({searchInputValue: this.props.formatInputText(title, subtitle)})
  }

  handleChangeInput(e) {
    const input = e.target.value
    this.changeInput(input)

    if (this.prepareSearchTerm(input).length < this.props.minInputCharactersToAutocomplete) {
      //clear autocompletion, if input has less then required amount of chars
      this.stopAutocompletion()
      this.props.dispatch(clearAutoCompletion(this.props.componentId))
      return
    }

    this.startAutocompletion(input)
  }

  //gets selected autocomletion item data and changes the search input with the values
  handleAutocompletionItemClicked(itemId) {
    const id = Math.trunc(itemId)
    const item = this.props.data.autoCompletion.data.find(a => a.id === id)
    if (!item)
      return

    this.changeInput(item.title, item.subtitle)
    this.props.dispatch(onSearchItemSelected(this.props.componentId, this.props.searchType, item))
  }

  handleFocused(e) {
    this.setState({isFocused: true})
  }

  handleBlur(e) {
    this.stopAutocompletion()
    this.setState({isFocused: false})
  }

  render() {
    const inputId = 'input-search-' + this.props.searchId
    const wrapperStyle = {...this.props.wrapperStyle, width: '100%', textAlign: 'center', margin: '100px auto'}

    const { searchResults } = this.props.data

    return (
      <div style={wrapperStyle}>
        <div class="form-group" style={{maxWidth: '400px', margin: '0 auto', textAlign: 'center'}}>
          <label class="col-form-label col-form-label-lg" for={inputId}>{this.props.title}</label>
          <input
            class="form-control form-control-lg"
            value={ this.state.searchInputValue }
            type="text"
            placeholder={this.props.placeholder}
            id={inputId}
            onChange={this.handleChangeInput.bind(this)}
            onFocus={this.handleFocused.bind(this)}
            onBlur={this.handleBlur.bind(this)}/>

            {
              //rendering search loading icon
              this.props.data && this.props.data.autoCompletion && this.props.data.autoCompletion.isFetching ?
                (
                  <div class="progress" style={{width: '100%', height: '5px', borderRadius: '0', marginTop: '-5px', borderBottomLeftRadius: '30px', borderBottomRightRadius: '30px'}}>
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{width: '100%'}}>
                    </div>
                  </div>
                ) :
                null
            }

            <SearchAutocompletionPopup
              searchComponentId={this.props.componentId} //setting autocompletion searchComponentId to own componentId, so that will be bind components data
              isFocused={this.state ? this.state.isFocused : false}
              onItemSelected={this.handleAutocompletionItemClicked.bind(this)} //callback when autocompleted item was clicked/selected
            />
        </div>
      </div>
    )
  }
}

SearchWithAutocompletion.propTypes = {
  componentId:  React.PropTypes.string.isRequired,
  searchAutocompletionDelay: React.PropTypes.number,
  maxAutocompletionResults: React.PropTypes.number,
  minInputCharactersToAutocomplete: React.PropTypes.number,
  formatInputText:  React.PropTypes.func
};
SearchWithAutocompletion.defaultProps = {
  searchAutocompletionDelay: 500, //how long to wait until start autocompletion search after adding a character
  maxAutocompletionResults: 6, //max suggested autocompletion items
  minInputCharactersToAutocomplete: 3, //how many characters are required to start autocompletion search
  formatInputText: ((title, subtitle) => title), //by default, we get rid of subtitle
}
