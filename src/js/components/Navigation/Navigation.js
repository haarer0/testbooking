import React from "react";
import { IndexLink, Link } from "react-router";

import NavigationLink from './NavigationLink'

export default class Navigation extends React.Component {
  constructor(props, context) {
    super(props, context)
    this.state = {
      ...this.state,
      collapsed: true
    }
  }

  //mobile designs friendly. overriding bootswatch's js burger-click function, which was written using jQuery
  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const collapsedClass = this.state.collapsed ? ' collapse' : '';
    return (

      <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary" role="navigation">
        <div class="container">
          <IndexLink to="/" class="navbar-brand">Solux Tickets</IndexLink>
          <button class="navbar-toggler" type="button" data-toggle="collapse" onClick={this.toggleCollapse.bind(this)} data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div className={'navbar-collapse' + collapsedClass} id="navbarResponsive">
            <ul class="navbar-nav">
              {this.props.navLinks.map((el, index) => <NavigationLink linkData={el} key={index}></NavigationLink>)}
            </ul>

            <ul class="nav navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="https://google.com/" target="_blank">Learn more</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="https://stackoverflow.com/" target="_blank">FAQ</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

Navigation.propTypes = { navLinks: React.PropTypes.array };
Navigation.defaultProps = {
  navLinks: [
    { link: '/', title: 'Main'},
    { link: '/flights', title: 'Flights'},
    { link: '/hotels', title: 'Hotels'},
    { link: '/cars', title: 'Cars'},
    { link: '/discover', title: 'Discover'},
    { link: '/deals', title: 'Deals'}
  ]};
