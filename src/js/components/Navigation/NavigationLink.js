import React from "react";
import { IndexLink, Link } from "react-router";

export default class NavigationLink extends React.Component {
  renderLinkComponent() {
    const {linkData} = this.props;

    if (linkData.link === '/')
      return (<IndexLink activeClassName='active' class="nav-link" to='/'>{linkData.title}</IndexLink>);
    else
      return (<Link activeClassName='active' class="nav-link" to={linkData.link}>{linkData.title}</Link>);
  }

  render() {
    return (
      <li className='nav-item'>
        { this.renderLinkComponent() }
      </li>
    )
  }
}
