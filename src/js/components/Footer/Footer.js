import React from "react";


export default class Footer extends React.Component {
  render() {
    return (
      <footer style={{ padding: '15px 0 0', textAlign: 'center', color: '#fff' }} class="fixed-bottom navbar-dark bg-primary">
        <p>Copyright &copy; Dmytro Korniienko</p>
      </footer>
    );
  }
}
