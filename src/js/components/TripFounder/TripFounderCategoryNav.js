import React from "react";

//thisis a simple tab navigation. Nav items can be active, if selected or disabled, if no tab data was received (controlled by parent comp via isDisabled prop)
export default class TripFounderCategoryTab extends React.Component {
  handleClick(e) {
    e.preventDefault();

    if (!this.props.isActive && !this.props.isDisabled)
      this.props.onClick(this.props.tabId);
  }

  render() {
    const tabId = this.props.tabId
    const extraCSSClasses = (this.props.isActive ? ' active' : '') + (this.props.isDisabled ? ' disabled' : '');

    return (
      <li class="nav-item">
        <a //have to use <a> because of bootswatch styles
          class={'nav-link' + extraCSSClasses}
          data-toggle="tab"
          data-tabid={tabId}
          href='javascript:void(0)'
          onClick={this.handleClick.bind(this)}
        >
          {this.props.title}
        </a>
      </li>
    );
  }
}
