import React from "react";
import { connect } from "react-redux"

import { TRIPFOUNDER_CATEGORIES } from './TripFounder'

@connect((store, ownProps) => {
  return {
    tripCategoryData: store.tripFounderReducer.tripData.data ? store.tripFounderReducer.tripData.data[ownProps.dataPropertyName] : null //failback in case there was an error
  }
})

//this is an semi-abstract class which takes a category type and renders tab content depending on the type
export default class TripFounderCategoryContainer extends React.Component {
  renderCategory() {
    const {info, list} = this.props.tripCategoryData

    switch (this.props.dataPropertyName) {
      //rendering possible flight directions
      case TRIPFOUNDER_CATEGORIES.FLIGHTS : {
        const departureCity = info.departureCity

        return list.map((arrivalCity, index) => (
          <div class="row" key={index}>
            <div class="col-lg-5" key={index + 'col1'}>
              <div class="card border-primary mb-3" style={{maxWidth: '20rem'}} key={index}>
                <div class="card-header" key={index + 'head'}>Departure</div>
                <div class="card-body text-primary" key={index + 'body'}>
                  <h4 class="card-title" key={index + 'h4'}>{departureCity.cityName}, {departureCity.countryName}</h4>
                  <p class="card-text" key={index + 'p'}>{departureCity.airportName} ({departureCity.airportIATA})</p>
                </div>
              </div>
            </div>

            <div class="col-lg-2" key={index + 'col2'}>
              <div class="mb-3" style={{maxWidth: '20rem', textAlign: 'center', height: '100%', paddingTop: '20%'}} key={index}>
                <p key={index + 'p1'}>=></p>
                <p key={index + 'p2'}>{ arrivalCity.schedule.join(', ') } </p>
                <p key={index + 'p3'}>=></p>
              </div>
            </div>

            <div class="col-lg-5" key={index + 'col3'}>
              <div class="card border-success mb-3" style={{maxWidth: '20rem'}} key={index}>
                <div class="card-header" key={index + 'head'}>Arrival</div>
                <div class="card-body text-success" key={index + 'body'}>
                  <h4 class="card-title" key={index + 'h4'}>{arrivalCity.cityName}, {arrivalCity.countryName}</h4>
                  <p class="card-text" key={index + 'p'}>{arrivalCity.airportName} ({arrivalCity.airportIATA})</p>
                </div>
              </div>
            </div>
          </div>
        ))
      }

      //due to lack of time I put just a placeholders for the remaining categories
      case TRIPFOUNDER_CATEGORIES.HOTELS :
      case TRIPFOUNDER_CATEGORIES.CARS :
      case TRIPFOUNDER_CATEGORIES.OFFERS : {
        return (
          <div class="row">
            <div class="col-lg-12 mb-3">{info.description}</div>
            { list.map((el, index) => (
                <div class="col-lg-5" key={index}>
                  <div class="card border-primary mb-3" style={{maxWidth: '20rem'}} key={index}>
                    <div class="card-header" key={index}>{el.name}</div>
                  </div>
                </div>
              )
            )}
          </div>
        )
      }
    }

    return null
  }

  render() {
    if (!this.props.tripCategoryData) return null

    return (
      <div class="tab-content">
        {this.renderCategory()}
      </div>
    );
  }
}

TripFounderCategoryContainer.propTypes = {
  dataPropertyName: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired
};
