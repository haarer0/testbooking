import React from "react"
import { connect } from "react-redux"

import { fetchFoundTripData } from '../../actions/tripFounderActions'

import TripFounderCategoryNav from './TripFounderCategoryNav'

export const TRIPFOUNDER_CATEGORIES = {
  FLIGHTS: 'flights',
  HOTELS: 'hotels',
  CARS: 'cars',
  OFFERS: 'offers'
}

@connect((store, ownProps) => {
  return {
    /*
    binding search data only to SPECIFIC search component (by its id). no other event data will be stored here
    this can be easily checked by adding and selecting from another search component with a different componentId on the page
    */
    searchData: store.tripFounderReducer.searchData,
    tripData: store.tripFounderReducer.tripData
  }
})

//this component is a Tab with navigation and active tab content subcomponents.
//It is persistant across pages, so we don't need to make a separate call if users comes back to the page with this component
export default class TripFounder extends React.Component {
  constructor(props, context) {
      super(props, context);
      this.state = {
          activeTabIndex: 0,
          isVisible: false
      };
  }

  componentWillReceiveProps(nextProps, nextState) {
    const searchData = nextProps.searchData ? nextProps.searchData[this.props.citiesSearchComponentId] : null
    if (!searchData) return

    //we will fetch data if only city was selected in a given autocompletion search component (and searchData will be binded via store connect)
    if (searchData.isCitySelected) {
      this.props.dispatch(fetchFoundTripData(searchData.cityId))
    }
  }

  setActiveTab(tabIndex) {
    this.setState({ activeTabIndex: tabIndex === this.state.activeTabIndex ? 0 : tabIndex });
  }

  renderTabNav() {
    const { tripData } = this.props

    //rendering tab nav using a separate component
    return React.Children.map(this.props.children, (child, index) => {
      return (
        <TripFounderCategoryNav
          title={child.props.title}
          tabId={index}
          key={index}
          isActive={index === this.state.activeTabIndex}
          isDisabled={!tripData || !tripData.isFetched || !tripData.data[child.props.dataPropertyName]}
          onClick={this.setActiveTab.bind(this)}/>
      )
    });
  }

  render() {
    if (!this.props.tripData)
      return null

    if (this.props.tripData.isFetching) {
      return (
        <div class="progress" style={{width: '100%', height: '5px', borderRadius: '0', marginTop: '-5px', borderBottomLeftRadius: '30px', borderBottomRightRadius: '30px'}}>
          <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style={{width: '100%'}}>
          </div>
        </div>
      )
    }

    if (this.props.tripData.isFetched) {
      return (
        <div class="main-tabs col-lg-12" style={{width: '100%', marginBottom: '100px'}}>
          <ul class="nav nav-tabs">
            {this.renderTabNav()}
          </ul>

          <div id="mainTabContent" class="tab-content" style={{ padding: '15px 5px 5px' }}>
            { this.props.children[this.state.activeTabIndex] }
          </div>
        </div>
      )
    }

    if (this.props.tripData.error) {
      return (
        <div class="main-tabs col-lg-12" style={{width: '100%', marginBottom: '100px'}}>
          <div class="alert alert-dismissible alert-danger">
            <strong>Oh snap!</strong> Server has returned an error! We are deeply sorry. Please try again later
          </div>
        </div>
      )
    }
    return null
  }
}

TripFounder.propTypes = {
  citiesSearchComponentId:  React.PropTypes.string.isRequired, //link to autocompletion search component, so we will bind searchData to its search results
}
